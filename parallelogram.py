from collections import Counter
N =  int(input('Enter number of lines'))
angles = []
for i in range(N):
    angles.append(int(input()))
counts = Counter(angles)
parallels = []
for ele,count in counts.items():
    if(count>1):
        parallels.append((ele,count))
    print("Element: " , ele, " and count : ", count)
print(parallels)