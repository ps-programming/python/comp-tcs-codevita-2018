TENPOWSEVEN = 10000000
TENPOWEIGHTEEN = 1000000000000000000

def createfile(inputString):
    with open('inputs.txt', 'w') as f:
        numbers = inputString.split()
        for number in numbers:
            f.write('\n%s' % number)
sum = 0
N = int(input())
prev_nums = set()
if(1<=N and N <= TENPOWSEVEN):
    createfile(input())
    with open('inputs.txt', 'r') as f:
        f.readline()
        num = int(f.readline().strip('\n'))
        while(num):
            if(not num in prev_nums):
                prev_nums.add(num)
                if(1 <= num and num <= TENPOWEIGHTEEN ):
                    power = 2
                    for i in range(num-1):
                        power = power * 2                 
                    power = power % 100
                    sum += power
            nextnum = f.readline().strip()
            if(not nextnum == ''):
                num = int(nextnum)
            else:
                break
print(sum % 100)


# else:
#     print("N should be greater or equal to 1 and less than or equal to 10^7")

