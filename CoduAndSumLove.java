import java.util.Scanner;

class CoduAndSumLove {
    public static void main(String[] args) {
        final long TENPOWSEVEN = 10000000;
        final long TENPOWEIGHT = 100000000;
        Scanner sc = new Scanner(System.in);

        long sum = 0;

        long N = sc.nextLong();
        if(1<=N && N <= TENPOWSEVEN)
        {
            int i = 0;
            while ( i < N ) {
                final long x = sc.nextLong(); // read input
                if(1 <= x && x <= TENPOWEIGHT ){
                    long power = (long) Math.pow(2, x);
                    power = power % 100;
                    sum += power;
                    i++;
                }
                else{
                    System.out.println("Enter a value between 1 and 10^8 for x");
                }
            }
    
            System.out.println(sum % 100);
        }
        else {
            System.out.println("N should be greater or equal to 1 and less than or equal to 10^7");
        }
        sc.close();

        
    }
}