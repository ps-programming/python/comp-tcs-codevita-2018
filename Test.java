/**
 * Test
 */
import java.util.Scanner;
public class Test {

    // public static void main(String[] args) {
    // long x = 1<<1;
    // System.out.println(x + "");
    // String str = "heads";
    // System.out.println(str.substring(str.length()-2));
    // }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        long sum = 0;

        int N = sc.nextInt();

        for (int i = 0; i < N; i++) {

            final long x = sc.nextLong(); // read input

            String str = Long.toString((long) Math.pow(1 << 1, x));

            str = str.length() > 2 ? str.substring(str.length() - 2) : str;

            sum += Integer.parseInt(str);

        }

        System.out.println(sum % 100);

    }
}